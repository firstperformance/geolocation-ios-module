![FPGLocation](./FPGLocation.png)

# FPGLocation
A framework for collecting and reporting user locations.  GPS coordinates enhance the First Performance platform and allow for smarter fraud prevention and merchant data cleansing.

See example app [here](https://bitbucket.org/firstperformance/geolocation-ios-app-example).

## Table of Contents
* [FPGLocation](#fpglocation)
  * [Requirements](#requirements)
  * [Installation](#installation)
    * [Swift Package Manager](#swift-package-manager)
      * [Add to Xcode](#add-to-xcode)
      * [Add to Swift Package](#add-to-swift-package)
    * [Manual Installation](#manual-installation)
  * [Setup](#setup)
    * [Add Usage Descriptions](#add-usage-descriptions)
    * [Request Background Permission](#request-background-permission)
  * [Usage](#usage)
    * [Initialization](#initialization)
    * [Getting Locations](#getting-locations)
    * [Customization](#customization)

## Requirements

- iOS 11.0+
- Xcode 11.0+
- Swift 5+

## Installation

### Swift Package Manager

The [Swift Package Manager](https://swift.org/package-manager/) is a tool for automating the distribution of Swift code and is integrated into the `swift` compiler.

#### Add to Xcode
You can add FPGLocation to your project through the Xcode by selecting `File > Swift Packages > Add Package Dependency` and searching for `git@bitbucket.org:firstperformance/geolocation-ios.git`. 

#### Add to Swift Package
You can add FPGLocation to your own Swift package by adding it to the `dependencies` value of your `Package.swift`.

```swift
dependencies: [
  .package(url: "https://bitbucket.org/firstperformance/geolocation-ios-module.git", from: "1.0.0")
]
```

### Manual Installation

You can manually add FPGLocation to your project by downloading the contents of this repository and dragging the folder to the project workspace in Xcode.

To add the FPGLocation package as local package:

- Check out FPGLocation package's source code from this repository.
- Open your app’s Xcode project or workspace.
- Select the FPGLocation folder in Finder and drag it into the Project navigator. This action adds FPGLocation’s Swift package as a local package to your project.

## Setup

FPGLocation can work with any configuration of location usage permissions, but works best when granted "Always" permission.  

### Add Usage Descriptions

Add these keys to your `Info.plist` along with an explanation of how your app uses the user's location data.

```
Privacy - Location Always and When In Use Usage Description
Privacy - Location When In Use Usage Description
```
### Request Background Permission

You must also indicate that background location updates are required if you want FPGLocation to continue to receive location events in the background.

You can add the "Location updates" Background Mode through Xcode, or add this key directly to your `Info.plist`:

```xml
<key>UIBackgroundModes</key>
<array>
    <string>location</string>
</array>
```
## Usage

#### Initialization

In order to start using FPGLocation, some inital setup is required.

```swift
// Create and retain a LocationManager. The base URL for the remote endpoint and 
// an x-auth token must be provided during initialization.
// The geolocation API path will be appeneded to the specified URL.
let locationManager = LocationManager(baseURL: URL, token: String)

// You then need to set your clientUserId
locationManager.clientUserId = <your-clientUser-id>

// If you want to be notified of location events and errors, you need to 
// tell LocationManager where to send those updates via it's delegate property. 
// LocationManager will process locations and send them to the remote server
// even if it has no delegate set.
locationManager.delegate = <conforming-class>
```

#### Getting Locations

LocationManager will not begin monitoring for locations until told to do so.  Updates will continue until LocationManager is stopped.  Switching to and from background mode is handled automatically.

```swift
// To start receiving locations
locationManager.start()

// To stop receiving locations
locationManager.stop()
```

#### Customization

Some of the properties of LocationManager can be tuned to better fit the needs of your app.
The default minimumUpdateInterval is 30 seconds, and the default distanceFilter is 3 meters.

```swift
// Minimum time in minutes before saving a new location
locationManager.minimumUpdateInterval = 0.5 // 30 seconds

// The horizontal distance in meters required to trigger a new event
public var distanceFilter: Double = 3.0 // 3 meters
```
