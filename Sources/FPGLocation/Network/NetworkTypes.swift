//
//  NetworkTypes.swift
//  FPGLocation
//
//  Created by Mathew Bryn Trussell on 8/21/19.
//  Copyright © 2019 First Performance Global. All rights reserved.
//

import Foundation

/// The types of results from network requests
internal enum NetworkResult {
    case success
    case error(Error)
}

/// Errors that could occur when handling network requests and responses
public enum NetworkError: Error, Equatable {

    // whenever building the request fails
    case invalidRequest

    // if the response is not present
    case invalidResponse

    // if the response is not a 200
    case httpError(code: Int)

    // everything else
    case unknown(error: Error?)
}

public func ==(lhs: NetworkError, rhs: NetworkError) -> Bool {
    switch (lhs, rhs) {
    case (.invalidRequest, .invalidRequest):
        return true
    case (.invalidResponse, .invalidResponse):
        return true
    case (let .httpError(leftCode), let .httpError(rightCode)):
        return leftCode == rightCode
    case (.unknown, .unknown):
        return true
    default:
        return false
    }
}
