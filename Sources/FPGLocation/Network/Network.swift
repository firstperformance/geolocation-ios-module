//
//  Network.swift
//  FPGLocation
//
//  Created by Mathew Bryn Trussell on 8/21/19.
//  Copyright © 2019 First Performance Global. All rights reserved.
//

import Foundation

/**
 Handles sending the URLRequest with the location data to the server
 */
internal class Network: OSLogger {

    // MARK: - Route Info
    // There's only one endpoint for geolocation currently
    // so putting this here is fine until more routes exist
    private let locationPath = "geolocation/%1$@"
    private let locationHTTP = "POST"

    // MARK: - Completion Type
    typealias NetworkCompletion = ((NetworkResult) -> Void)?

    // MARK: - URL Session

    private let session: URLSession
    private let baseURL: URL

    // MARK: - Public

    init(baseURL: URL, token: String) {

        // Default URLSession
        let config = URLSessionConfiguration.default

        // Allow iOS to schedule the requests when convenient
        config.isDiscretionary = true

        // Fail Quickly
        config.timeoutIntervalForRequest = 1.0
        config.waitsForConnectivity = false

        // Add token to every request
        config.httpAdditionalHeaders = ["X-Auth-Token" : token]

        self.session = URLSession(configuration: config)
        self.baseURL = baseURL
    }

    func save(location: Location, completion: NetworkCompletion) {

        guard let request = buildRequest(for: location) else {
            completion?(.error(NetworkError.invalidRequest))
            return
        }

        send(request: request, completion: completion)
    }

    // MARK: - URL Request
    private func buildRequest(for location: Location) -> URLRequest? {

        let encoder = JSONEncoder()
        guard let jsonData = try? encoder.encode(location)
            else { return nil }

        let path = String(format: locationPath, location.clientUserId)
        let url = baseURL.appendingPathComponent(path)

        var request = URLRequest(url: url)
        request.httpMethod = locationHTTP
        request.httpBody = jsonData

        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")

        return request
    }

    private func send(request: URLRequest, completion: NetworkCompletion) {

        log("Request URL: \(request.url)", log: Log.network, level: .debug)
        log("Request Body: \(request.httpBody)", log: Log.network, level: .debug)

        let task = session.dataTask(with: request) { (data, response, error) in

            guard error == nil else {
                completion?(.error(NetworkError.unknown(error: error)))
                return
            }

            guard let http = response as? HTTPURLResponse else {
                completion?(.error(NetworkError.invalidResponse))
                return
            }

            guard case 200 ... 299 = http.statusCode else {
                completion?(.error(NetworkError.httpError(code: http.statusCode)))
                    return
            }

            completion?(.success)
        }

        task.resume()
    }
}
