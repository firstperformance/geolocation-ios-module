//
//  LocationManager.swift
//  FPGLocation
//
//  Created by Mathew Bryn Trussell on 8/21/19.
//  Copyright © 2019 First Performance Global. All rights reserved.
//

import Foundation
import CoreLocation

/**
 The methods that you use to receive events from an associated location manager object.
 */
public protocol LocationManagerDelegate: class {

    /// Called when a new location is recorded.
    func locationManager(_ manager: LocationManager, didRecord location: Location)

    /// Called whenever an error occurs with location updates.
    func locationManager(_ manager: LocationManager, didFailWith error: LocationManagerError)

    /// Called whenever there is a change in Location Services authorization.
    func locationManager(_ manager: LocationManager, didChange state: LocationAuthorizationState)
}

/**
 Public interface for integrating location tracking. Determines whether location updates
 should be monitored and if changes are significant enough to save.
 */
final public class LocationManager: CoreManagerDelegate, OSLogger {

    // MARK: - Configuration

    /// Minimum time in minutes to ignore updates before saving a new location
    public var minimumUpdateInterval: Double = 2

    /// ClientUser's unique identifier. Required for use!
    public var clientUserId: String? {
        didSet {
            // Forward the clientUser id to the CoreManager so that
            // CLLocations can be converted to Location structs
            core.clientUserId = clientUserId
        }
    }

    /// The horizontal distance in meters required to trigger a new event
    public var distanceFilter: Double = 200.0 {
        didSet {
            // Forward the distance filter to the CoreManager so that
            // CoreLocation can set it's own distanceFilter property
            core.distanceFilter = distanceFilter
        }
    }

    /// The delegate class to provide with updates
    public weak var delegate: LocationManagerDelegate?

    // MARK: - Location Monitoring

    /// Indicates whether location monitoring is active or not.
    /// This will be true after calling `start` and false after calling `stop`
    public var isUpdatingLocation: Bool { return core.isUpdatingLocation }

    /// Indicates whether the app is currently monitoring for location changes
    /// in the background. This is always false if `isUpdatingLocation` is false.
    public var isUpdatingInBackground: Bool { return core.isUpdatingInBackground }

    /// Start location monitoring.
    public func start() {
        lastLocation = nil
        core.startUpdates()
    }

    /// Stop location monitoring.
    public func stop() {
        lastLocation = nil
        core.stopUpdates()
        core.stopBackgroundUpdates()
    }

    // MARK: - Init

    /// A CoreManager object to handle interacting with CoreLocation.
    private let core = CoreManager()

    /// The url session to use for saving locations.
    private let network: Network

    public init(baseURL: URL, token: String) {
        network = Network(baseURL: baseURL, token: token)

        core.delegate = self
        core.distanceFilter = distanceFilter
    }

    // MARK: - Location Recording

    /// ClientUser's last saved location.
    private var lastLocation: Location?

    /**
     Determines if enough time has passed to save the latest location change.

     - Parameter now: The Date to compare against the timestamp of the last location.
                      Assumed to be the current time.
     - Returns: `true` if the difference between the two dates is
                greater than the `minimumUpdateInterval`.
     */
    private func isItTime(_ now: Date) -> Bool {
        guard let last = lastLocation else { return true }

        // get the time past in minutes
        let timePast = now.timeIntervalSince(last.timestamp) / 60

        // true if timePast is more than the minimum interval
        return timePast >= minimumUpdateInterval
    }

    /**
     Saves the given location, sends it to the server, and tells
     the LocationManagerDelegate a new location was recorded.

     - Parameter location: The Location to save
     - Returns: `true` if key exists in the infoDictionary
     */
    private func recordLocation(_ location: Location) {
        log(Log.Text.savingLocation, log: Log.location, level: .debug)

        validateDelegate()
        delegate?.locationManager(self, didRecord: location)
        lastLocation = location

        network.save(location: location, completion: { result in
            guard case let .error(err) = result else { return }
            let serverError = LocationManagerError.serverError(err)
            self.delegate?.locationManager(self, didFailWith: serverError)
        })
    }

    // MARK: - Core Manager Delegate
    func didUpdate(_ location: Location) {
        log(#function, log: Log.location, level: .debug)

        // If there is no saved location always save the new location
        guard lastLocation != nil else {
            // No previous location so save this one
            recordLocation(location)
            return
        }

        // Get the current timestamp for comparison
        let now = Date()

        // Check that the time passed exceeds the minimum threshold
        guard isItTime(now) else {
            // Not enough time has passed
            log(Log.Text.notEnoughTime, log: Log.location, level: .debug)
            return
        }
        
        let currentCoordinates = CLLocation(latitude: location.latitude, longitude: location.longitude)
        let lastCoordinates = CLLocation(latitude: lastLocation?.latitude ?? 0.0, longitude: lastLocation?.longitude ?? 0.0)
        let distanceInMeters = currentCoordinates.distance(from: lastCoordinates)
        log("Distance from last location = \(distanceInMeters)", log: Log.location, level: .debug)
        
        if distanceInMeters < distanceFilter {
            // Not enough distance has been covered
            log(Log.Text.notEnoughDistance, log: Log.location, level: .debug)
            return
        }

        // New location is significant and within time frame
        recordLocation(location)
    }

    func didFail(_ error: LocationManagerError) {
        log(#function, log: Log.location, level: .debug)
        validateDelegate()
        delegate?.locationManager(self, didFailWith: error)
    }

    func didChange(_ state: LocationAuthorizationState) {
        log("\(#function) - State is \"\(state)\"", log: Log.location, level: .debug)
        validateDelegate()
        delegate?.locationManager(self, didChange: state)
    }

    // Logs a warning if the delegate is nil
    private func validateDelegate() {
        guard delegate == nil else { return }
        log("LocationManagerDelegate is nil", log: Log.location, level: .info)
    }
}
