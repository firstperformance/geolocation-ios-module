//
//  CoreManager.swift
//  FPGLocation
//
//  Created by Mathew Bryn Trussell on 8/21/19.
//  Copyright © 2019 First Performance Global. All rights reserved.
//

import CoreLocation
import UIKit

/**
 The methods that you use to receive events from an associated core manager object.
 */
internal protocol CoreManagerDelegate: class {

    /// Called when CoreLocation reports a new location.
    func didUpdate(_ location: Location)

    /// Called when CoreLocation reports an error.
    func didFail(_ error: LocationManagerError)

    /// Called when CoreLocation reports a change in the app's authorization status.
    func didChange(_ state: LocationAuthorizationState)
}

/**
 Primary interface around the CoreLocation framework. Handles the
 CLLocationManager lifecycle, and forwards relevant updates from the
 CLLocationManagerDelegate protocol to its own CoreManagerDelegate
 (which is probably a LocationManager).
 */
internal class CoreManager: NSObject, CLLocationManagerDelegate, OSLogger {

    /// CoreLocation events are forwarded to this object
    weak var delegate: CoreManagerDelegate?

    /// This unique identifier is inherited from the LocationManager class
    /// and is added to each recorded Location struct
    var clientUserId: String?

    /// This value is inherited from the LocationManager class and is used to set the
    /// distanceFilter property of the CLLocationManager.
    var distanceFilter: Double? {
        didSet {
            // Update the CLLocationManager when this value is changed
            guard let filter = distanceFilter else { return }
            manager.distanceFilter = filter
        }
    }

    /// Indicates if the CLLocationManager is "on" and delivering location updates.
    /// This will be true once either `startUpdates()` or `startBackgroundUpdates()` is called.
    internal private(set) var isUpdatingLocation = false

    /// Indicates if the app is in the background.
    /// This will be false if `isUpdatingLocation` is false.
    /// This will only be true if `startBackgroundUpdates` is called.
    internal private(set) var isUpdatingInBackground = false

    /// A CoreLocationManager to get location information from iOS.
    private let manager = CLLocationManager()

    // MARK: - Init

    internal override init() {
        super.init()
        self.manager.delegate = self
        self.manager.pausesLocationUpdatesAutomatically = false
        self.manager.activityType = .fitness

        if self.manager.hasBackgroundLocationMode {
            self.manager.allowsBackgroundLocationUpdates = true
        } else {
            log(Log.Text.backgroundRequired, log: Log.coreloc, level: .error)
        }

        // Set up the notification center observers
        addObservers()
    }

    deinit {
        // Clean up the notification center observers
        removeObservers()
    }

    // MARK: - Notification Center

    /**
     Add observers to NotificationCenter to watch for the app
     moving into background and foreground.
     */
    private func addObservers() {
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.applicationEnteredBackground),
                                               name: UIApplication.didEnterBackgroundNotification,
                                               object: nil)
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.applicationEnteringForeground),
                                               name: UIApplication.willEnterForegroundNotification,
                                               object: nil)
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.applicationWillTerminate),
                                               name: UIApplication.willTerminateNotification,
                                               object: nil)
    }

    /**
     Removes NotificationCenter observers that are added in `addObservers`.
     */
    private func removeObservers() {
        NotificationCenter.default.removeObserver(self,
                                                  name: UIApplication.didEnterBackgroundNotification,
                                                  object: nil)
        NotificationCenter.default.removeObserver(self,
                                                  name: UIApplication.willEnterForegroundNotification,
                                                  object: nil)
        NotificationCenter.default.removeObserver(self,
                                                  name: UIApplication.willTerminateNotification,
                                                  object: nil)
    }

    /**
     When the app enters the background, if location monitoring was started previously,
     switch the CLLocationManager to background mode.
     */
    @objc func applicationEnteredBackground() {
        log(Log.Text.appInBackground, log: Log.coreloc, level: .default)
        guard isUpdatingLocation else { return }
        startBackgroundUpdates()
    }

    /**
     When the app becomes active, if location monitoring was started previously,
     switch the CLLocationManager to active mode.
     */
    @objc func applicationEnteringForeground() {
        log(Log.Text.appIsActive, log: Log.coreloc, level: .default)
        guard isUpdatingLocation else { return }
        startUpdates()
    }

    /**
     When the app is about to terminate, switch to monitoring significant changes only.
     This is supposed to survive a restart
     */
    @objc func applicationWillTerminate() {
        log(Log.Text.appIsTerminating, log: Log.coreloc, level: .info)
        guard isUpdatingLocation else { return }
        startSignificantUpdates()
    }

    // MARK: Manager Life Cycle

    func startUpdates() {
        log(#function, log: Log.coreloc, level: .debug)
        requestAuthorizationIfNeeded()
        stopBackgroundUpdates()

        manager.desiredAccuracy = kCLLocationAccuracyBest
        manager.startUpdatingLocation()

        isUpdatingLocation = true
        isUpdatingInBackground = false
    }

    func stopUpdates() {
        log(#function, log: Log.coreloc, level: .debug)
        manager.stopUpdatingLocation()
        manager.stopMonitoringSignificantLocationChanges()
        isUpdatingLocation = false
        isUpdatingInBackground = false
    }

    func startBackgroundUpdates() {
        log(#function, log: Log.coreloc, level: .debug)
        requestAuthorizationIfNeeded()
        stopUpdates()

        guard manager.allowsBackgroundLocationUpdates else {
            log(Log.Text.backgroundRequired, log: Log.coreloc, level: .error)
            delegate?.didFail(LocationManagerError.backgroundModeRequired)
            return
        }

        manager.desiredAccuracy = kCLLocationAccuracyHundredMeters
        manager.startUpdatingLocation()

        isUpdatingLocation = true
        isUpdatingInBackground = true
    }

    func stopBackgroundUpdates() {
        log(#function, log: Log.coreloc, level: .debug)
        manager.stopUpdatingLocation()
        manager.stopMonitoringSignificantLocationChanges()
        isUpdatingLocation = false
        isUpdatingInBackground = false
    }

    func startSignificantUpdates() {
        log(#function, log: Log.coreloc, level: .debug)

        stopUpdates()
        stopBackgroundUpdates()

        manager.startMonitoringSignificantLocationChanges()

        isUpdatingLocation = true
        isUpdatingInBackground = true
    }

    /**
     Requests the authorization level given or uses the highest level found
     in the info plist, provided authorization has not already been determined.

     - Parameter level: The location usage type to request (Always or WhenInUse)
     */
    private func requestAuthorizationIfNeeded(_ level: LocationUsageType? = nil) {
        log(#function, log: Log.coreloc, level: .debug)
        let current = CLLocationManager.authorizationStatus()

        // Authorization status has been set unless the value is .notDetermined
        guard current == .notDetermined else { return }

        // Try to get the info plist value
        guard let plistLevel = manager.authLevelFromInfoPlist else {
            log(Log.Text.usageKeyMissing, log: Log.coreloc, level: .error)
            return
        }

        // Attempt to set the given level or find one in the plist
        let levelToSet = level ?? plistLevel

        switch levelToSet {
        case .always:
            log(Log.Text.requestAlways, log: Log.coreloc, level: .debug)
            manager.requestAlwaysAuthorization()
        case .whenInUse:
            log(Log.Text.requestWhenInUse, log: Log.coreloc, level: .debug)
            manager.requestWhenInUseAuthorization()
        }
    }

    // MARK: - CLLocationManager Delegate
    public func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        log(#function, log: Log.coreloc, level: .debug)

        guard let clientUser = clientUserId else {
            log(Log.Text.clientUserRequired, log: Log.coreloc, level: .error)
            delegate?.didFail(LocationManagerError.missingClientUserId)
            return
        }

        // The locations array is ordered chronologically ascending
        // so the last element is the most recent
        guard let location = locations.last else { return }

        delegate?.didUpdate(Location(location, clientUserId: clientUser))
    }

    public func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        log(#function, log: Log.coreloc, level: .debug)
        delegate?.didFail(LocationManagerError.coreLocationError(error))
    }

    public func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        log(#function, log: Log.coreloc, level: .debug)
        let state = manager.serviceState
        delegate?.didChange(state)
    }

    public func locationManagerDidPauseLocationUpdates(_ manager: CLLocationManager) {
        log(#function, log: Log.coreloc, level: .debug)
    }

    public func locationManagerDidResumeLocationUpdates(_ manager: CLLocationManager) {
        log(#function, log: Log.coreloc, level: .debug)
    }

}
