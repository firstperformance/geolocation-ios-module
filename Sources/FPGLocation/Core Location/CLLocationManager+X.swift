//
//  CLLocationManager+X.swift
//  FPGLocation
//
//  Created by Mathew Bryn Trussell on 8/21/19.
//  Copyright © 2019 First Performance Global. All rights reserved.
//

import CoreLocation

// MARK: CLLocationManager
extension CLLocationManager {

    /// The keys that represent location service permissions in the info plist.
    private struct UsageKeys {
        /// The key required to reqeust "Always" location permission.
        static let alwaysAndInUse = "NSLocationAlwaysAndWhenInUseUsageDescription"

        /// The key required to request "When In Use" location permission.
        static let inUse = "NSLocationWhenInUseUsageDescription"
    }

    /**
     Indicates whether the key "location" is set in the UIBackgroundMode dictionary within
     the Info.plist of the host application. CoreLocation requires this flag to continue
     delivering updates while the app is in the background.

     In Xcode 10.3, these keys appear as:
     "Required background modes": "App registers for location updates".
     The raw values should always be "UIBackgroundModes": "location".
     */
    var hasBackgroundLocationMode: Bool {
        guard let info = Bundle.main.infoDictionary,
            let modes = info["UIBackgroundModes"] as? [String]
            else { return false }

        return modes.contains("location")
    }

    /**
     Return the highest authorization level based upon the permission keys found in the
     Info.plist of the host application. The value will be nil if neither key is found.

     As of iOS 11, "NSLocationWhenInUseUsageDescription" must be present to use location services.
     If "Always" permission is required the Info.plist must also include the key  "NSLocationAlwaysAndWhenInUseUsageDescription".
     */
    var authLevelFromInfoPlist: LocationUsageType? {

        // Handle iOS 11 Location info keys
        let hasAlwaysAndWhenInUse = hasValue(forKey: UsageKeys.alwaysAndInUse)
        let hasWhenInUse = hasValue(forKey: UsageKeys.inUse)

        if hasAlwaysAndWhenInUse && hasWhenInUse {
            return .always
        } else if hasWhenInUse {
            return .whenInUse
        } else {
            return nil
        }
    }

    /**
     Checks that the specified value is set in the Info.plist of the host application.

     - Parameter key: The key to validate
     - Returns: `true` if key exists in the infoDictionary
     */
    private func hasValue(forKey key: String) -> Bool {
        guard let dict = Bundle.main.infoDictionary else { return false }
        return ((dict[key] as? String)?.isEmpty ?? true == false)
    }

    /**
     Get the current location services CLAuthorizationStatus
     and convert it to a LocationServiceState
     */
    var serviceState: LocationAuthorizationState {

        guard CLLocationManager.locationServicesEnabled() else {
            return .disabled
        }

        switch CLLocationManager.authorizationStatus() {
        case .notDetermined:
            return .notDetermined
        case .denied:
            return .denied
        case .restricted:
            return .restricted
        default:
            return .available
        }
    }
}

// MARK: CLAuthorizationStatus
extension CLAuthorizationStatus: CustomDebugStringConvertible {

    /// String descriptions to easily identify the CLAuthorizationStatus
    public var debugDescription: String {
        switch self {
        case .authorizedAlways:
            return "Always"
        case .authorizedWhenInUse:
            return "When In Use"
        case .denied:
            return "Denied"
        case .notDetermined:
            return "Not Determined"
        case .restricted:
            return "Restricted"
        @unknown default:
            return "Unknown CLAuthorizationStatus"
        }
    }
}

