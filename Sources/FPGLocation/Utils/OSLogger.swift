//
//  OSLogger.swift
//  geotest
//
//  Created by Mathew Bryn Trussell on 8/21/19.
//  Copyright © 2019 First Performance Global. All rights reserved.
//

import Foundation
import os.log

private let subsystem = Bundle.main.bundleIdentifier ?? "com.fpg.geo"

internal protocol OSLogger {
    func log(_ message: String, log: OSLog, level: OSLogType)
    func logPublic(_ message: String, log: OSLog, level: OSLogType)
}

extension OSLogger {
    func log(_ message: String, log: OSLog, level: OSLogType = .debug) {
        os_log("%@", log: log, type: level, message)
    }

    func logPublic(_ message: String, log: OSLog, level: OSLogType = .debug) {
        os_log("%{public}@", log: log, type: level, message)
    }
}

struct Log {
    // logs
    static let network = OSLog(subsystem: subsystem, category: "fp-network")
    static let location = OSLog(subsystem: subsystem, category: "fp-location")
    static let coreloc = OSLog(subsystem: subsystem, category: "fp-coreloc")

    struct Text {
        static let usageKeyMissing = "Location services in iOS 11+ requires a value for \"NSLocationWhenInUseUsageDescription\" in your Info.plist. To use \"always\" access you must provide a value for \"NSLocationAlwaysAndWhenInUseUsageDescription\" as well."

        static let backgroundRequired = "Background access is required"
        static let insufficientPermission = "Insufficient permission level"
        static let requestAlways = "Requesting Always authorization"
        static let requestWhenInUse = "Requesting WhenInUse authorization"
        static let clientUserRequired = "ClientUserId required"
        static let appInBackground = "App in background"
        static let appIsActive = "App is active"
        static let appIsTerminating = "App is terminating"
        static let savingLocation = "Saving Location"
        static let notEnoughTime = "Minimum time not expired"
        static let notEnoughDistance = "Minimum distance not expired"
    }
}
