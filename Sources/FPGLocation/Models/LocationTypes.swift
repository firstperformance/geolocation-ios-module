//
//  LocationTypes.swift
//  FPGLocation
//
//  Created by Mathew Bryn Trussell on 8/21/19.
//  Copyright © 2019 First Performance Global. All rights reserved.
//

import Foundation

/**
 Errors that could occur when using LocationManager.
 */
public enum LocationManagerError: Error, Equatable {

    /// Indicates CoreLocation generated an error.
    case coreLocationError(Error)

    /// Indicates background monitoring tried to start when backgroundMode is not available.
    case backgroundModeRequired

    /// Indicates the clientUserId is not set.
    case missingClientUserId

    /// Indicates the location was not saved on the server
    case serverError(Error)

    /// Indicates an unexpected error occured.
    case other(Error)
}

public func ==(lhs: LocationManagerError, rhs: LocationManagerError) -> Bool {
    switch (lhs, rhs) {
    case (.coreLocationError, .coreLocationError): return true
    case (.backgroundModeRequired, .backgroundModeRequired): return true
    case (.missingClientUserId, .missingClientUserId): return true
    case (.serverError, .serverError): return true
    case (.other, .other): return true
    default: return false
    }
}

/**
 The possible states Location Services can be in. This is analagous to `CLAuthorizationStatus`

 - available: User granted access to location services and they are enabled for use in the app. Applies to both "When In Use" and "Always" permission levels.

 - notDetermined: User has not yet responded to the dialog to grant permission.

 - denied: User has explicitly denied permission to access Location Services. User could re-enable services through the Settings app.

 - restricted: User does not have the ability to enabled location services due to a system setting like parental controls or device hardware capabilities.
 
 - disabled: User turned off location services for all apps in the Settings app.
 */
public enum LocationAuthorizationState: Int {
    case available
    case notDetermined
    case denied
    case restricted
    case disabled
}

/**
 Describes the possible levels of location service permissions that can be requested.

 - always: Access to location at all times (including background)
 - whenInUser: Access to location only when app is in the foreground
 */
enum LocationUsageType: Int {
    case always
    case whenInUse
}
