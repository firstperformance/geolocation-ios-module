//
//  ClientUserLocation
//  FPGLocation
//
//  Created by Mathew Bryn Trussell on 8/21/19.
//  Copyright © 2019 First Performance Global. All rights reserved.
//

import Foundation
import CoreLocation

/**
 Location is a light-weight container that combines the required data from CLLocation
 with the unique clientUserId.
 */
public struct Location: Codable, Hashable, Equatable {

    /// Latitude in degrees.
    public let latitude: Double

    /// Longitude in degrees.
    public let longitude: Double

    /// The direction of travel, measured in degrees and relative to due north.
    /// See `CLLocation.course` for more details.
    public let course: Double

    /// The time this location was reported.
    public let timestamp: Date

    /// A unique First Performance identifier.
    public let clientUserId: String

    public init(_ location: CLLocation, clientUserId: String) {
        self.clientUserId = clientUserId

        self.latitude = location.coordinate.latitude
        self.longitude = location.coordinate.longitude
        self.course = location.course
        self.timestamp = location.timestamp
    }
}
